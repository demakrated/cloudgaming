<?php

	set_include_path(get_include_path() . 'shell');
	require_once 'Net/SSH2.php';

	/**
	* Controlls and prepares commands to pass to the ShellScript. It tunnelizes
	* connections to the host that has the VM's and the ShellScript.
	*
	* @author Author: Daniel Pedrajas Van de Velde
	* @version 0.1
	*/
	class Shell
	{
		/**
		 * Settings from the .ini file with the connection settings fot the SSH Tunnel
		 * @var array
		 */
		private $settings;

		function __construct()
		{
			$this->settings = parse_ini_file('shell/shelltunnel.ini.php');

		}

		/**
		 * Fixes the correct command name for the Shell Script
		 * @return string name of the correct command for the ShellScript
		 */
		private function switchCommand(){
			switch ($this->command){
				case "status" :
					$this->command = "getvmstatus";
					break;
				case "on" :
					$this->command = "startvm";
					break;
				case "off" :
					$this->command = "stopvm";
					break;
				case "clone" :
					$this->command = "clonevm";
					break;
			}
		}

		/**
		 * Executes the correct ShellScript function. args sent to the ShellScript:
		 * $1 Function to execute
		 * $2 New VM name
		 * $3 VM name of the VM target to clone
		 * $4 Group Name of the new VM
		 * $5 Username owner of the new VM
		 * @param  array $args Array with the data for executing the correct ShellScript function
		 * @return string The ShellScript execution output
		 */
		public function execCommand($args){

			$ssh = new Net_SSH2($this->settings['host']);
			if (!$ssh->login($this->settings['user'], $this->settings['password'])) {
			    exit('Login Failed');
			}
			$this->command = $args['commandArg'];
			$this->switchCommand();
			//clonevm
			if($this->command == "clonevm"){
				return $ssh->exec($this->settings['script'] . ' ' . $args['commandArg']
						. ' ' . $args['vmName'] . ' ' . $args['vmTargetToclone'] . ' ' . $args['groupName']
						. ' ' . $args['userName']);
			//on and off
			}else{
				return $ssh->exec($this->settings['script'] . ' ' . $args['commandArg']  . ' ' . $args['vmName']);
			}
		}
	}
 ?>