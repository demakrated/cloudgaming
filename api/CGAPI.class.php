<?php

	require_once 'API.class.php';
	require_once 'controller/GameCTRL.class.php';
	require_once 'controller/UserCTRL.class.php';
	require_once 'shell/Shell.class.php';

	/**
	 * The CloudGaming API REST. Extends from the abstract Class API. Execute different
	 * Endpoints depending on the Verb of the request returning custom JSON Headers.
	 *
	 * @author Author: Daniel Pedrajas Van de Velde
	 * @version 0.1
	 */
	class CGAPI extends API
	{
	    //protected $User;

		/**
		 * The User or Game ID sent
		 * @var int
		 */
	    private $id;

	    /**
	     * The state sent
	     * @var string
	     */
	    private $state;

	    /**
	     * The data sent needed for make the digest authentication
	     * @var array
	     */
	    private $digestData;

	    public function __construct($request, $origin) {
	        parent::__construct($request);
	        $digestData = array('userID' => null, 'signature' => null, 'timestamp' => null);
	    }

	    /**
	     * Fills the state and the id from the request sent
	     * @param  array $args Array data
	     */
	    private function sanitizeArgs($args){
	    	if($args !== null && $this->verb == ""){
	    		if(count($args) >= 1){
	    			$this->id = $args[0];
	    		}
	    		if(count($args) >= 2){
	    			$this->state = $args[1];
	    		}
	    	}
	    }

	    /**
	     * Switch the correct state name given in the API query
	     * @throws Exception if the state sent is not correct
	     */
	    private function switchCorrectState(){
	    	switch ($this->state) {
	    		case 'on':
	    			$this->state = 'startvm';
	    			break;
	    		case 'off':
	    			$this->state = 'stopvm';
	    			break;
	    		case 'clone':
	    			$this->state = 'clonevm';
	    			break;
	    		case 'test':
	    			$this->state = 'test';
	    			break;
	    		default:
	    			throw new Exception("Error matching correct state", 1);
	    	}
	    }

	    /**
	     * Chekcs the headers for containing at least the needed ones
	     * @param  array $headers The headers array sent
	     * @throws Exception if wrong headers found
	     */
	    private function checkDigestParams($type = 'GET'){
	    	if($type == 'GET'){
	    		if(!isset($_GET['userID']) || !isset($_GET['signature'])){
	    			throw new Exception("Some headers are wrong. Need userID and signature
	    			to be present", 1);
	    		}
	    	}
	    	else if($type == 'POST'){
	    		if(!isset($_POST['userID']) || !isset($_POST['signature'])){
	    			throw new Exception("Some headers are wrong. Need userID and signature
	    			to be present", 1);
	    		}
	    	}
	    }

	    private function checkNewUserDigestData($type = null){
	    	if($type == 'GET'){
	    		if(!isset($_GET['username']) && !isset($_GET['password']) && !isset($_GET['timestamp'])){
	    			throw new Exception("Some headers are wrong. Need username, password and timestamp to be present", 1);
	    		}
	    	}
	    	else if($type == 'POST'){
	    		if(!isset($_POST['username']) && !isset($_POST['password']) && !isset($_POST['timestamp'])){
	    			throw new Exception("Some headers are wrong. Need username, password and timestamp to be present", 1);
	    		}
	    	}
	    }

	    /**
	     * Checks the headers sent and fills the needed data from it
	     */
	    private function fillDigestData($type = 'GET'){
	    	$this->checkDigestParams($type);
	    	if($type == 'GET'){
	    		$this->digestData['userID'] = $_GET['userID'];
	    		$this->digestData['signature'] = $_GET['signature'];
	    		$this->digestData['timestamp'] = $_GET['timestamp'];
	    	}
	    	else if($type == 'POST'){
	    		$this->digestData['userID'] = $_POST['userID'];
	    		$this->digestData['signature'] = $_POST['signature'];
	    		$this->digestData['timestamp'] = $_POST['timestamp'];
	    	}
	    	else if($type == 'PUT'){
	    		//order: signature, id, timestamp
	    		$data = $this->file;
	     		$data = explode(",", $data);
	     		$this->digestData['signature'] = $data[0];
	    		$this->digestData['userID'] = $data[1];
	    		$this->digestData['timestamp'] = $data[2];
	    	}
	    }

	    /**
	     * Makes the timestamp Digest of the User signature. Accepts a variance of +-5 minutes
	     * @param  int $userId The User ID
	     * @param  string $timestamp The User timestamp
	     * @return bool true if ok
	     * @throws Exception|mixed
	     */
	    private function makeTimestampDigest($userId = null, $timestamp = null){
	    	$digest = false;
	    	if($timestamp !== null && $userId !== null){

	    		$user = new User();
		    	$API_KEY = $user->getApiKey($userId);

		    	$date = new DateTime();
        		$current_timestamp = (string)$date->getTimestamp();
        		$realTimestamp = hash_hmac("sha256",$current_timestamp,$API_KEY);

        		//digest Timestamp between 5 minutes
        		$variance = $current_timestamp - $realTimestamp;
        		return $variance;
        		if($variance >= 300 || $variance <= -300)
        			throw new Exception("Error. Timestamp expired", 1);
		    	else{
		    		$digest = true;
		    	}

	    		return $digest;
	    	}
	    	else{
	    		throw new Exception("Signature and userID must be not null", 1);
	    	}
	    }

	     /**
	     * Makes the digest with username, userID and the API_KEY fot this user and session
	     * @param  string $signature sent by the user for the digest
	     * @param  int $userId of user
	     * @return bool return true if its correct. Else false
	     */
	    private function makeDigest($signature = null, $userId = null){

	    	$digest = false;
	    	if($signature !== null && $userId !== null){
	    		$user = new User();
		    	$API_KEY = $user->getApiKey($userId);
		    	$userName = $user->getName($userId);
		    	$realSig = hash_hmac("sha256",$userName.$userId,$API_KEY);
	    		if($signature === $realSig){
	    			$digest = true;
	    		}
	    		return $digest;
	    	}
	    	else{
	    		throw new Exception("Signature and userID must be not null", 1);
	    	}
	    }

	    /**
	     * Processes a PUT request for the resource game.
	     * Used for changing a VM instace state on or off: /api/game/?id/?state
	     * 1. Makes the digest for authentication with user signature hashed with his api key
	     * 2. Executes the correct ShellScript command for start or stop a VM
	     * 3. If its start, retrieves the VM IP and updates it in the DB
	     * @return string The ShellScript command execution output
	     */
	    private function processPUT(){

	    	//fill the digest data sent in the headers
     		$this->fillDigestData($this->method);
     		//Make the digest of the signature sent and the real signature
			if(!$this->makeDigest($this->digestData['signature'],$this->digestData['userID'])
				|| !$this->makeTimestampDigest($this->digestData['userID'],$this->digestData['timestamp'])){
				throw new Exception("Error in the Digest. Signature or userID are wrong", 1);
     		}

	    	if($this->id !== null){
        		//Change Game (VM) State
        		if($this->state !== null && ($this->state == "on" || $this->state == "off")){

					$game = new GameCtrl();
					$vmName = $game->getVMName($this->id);

					//Fix the correct state name for passing it to the script
					$this->switchCorrectState();

					//prepare args for the shell script
					// $1 -> commandArg
					// $2 -> vmName
					$args = array(
						'vmName' => $vmName,
						'commandArg' => $this->state
						);

					$shell = new Shell();
					if($this->state == "stopvm"){
						return $shell->execCommand($args);
					}
					else{
						//When starting the VM we need its IP offered by DHCP
						//First start the VM
						$shell->execCommand($args);
						//change executing command
						$args['commandArg'] = 'getvmip';
						//let the VM some time to recieve the IP
     					sleep(15);
     					//get its IP
     					$vmIP = $shell->execCommand($args);
     					//escape breaklines from the IP returned by the ShellScript
     					$vmIP = str_replace(array("\r\n", "\n", "\r"), ' ', $vmIP);

    					//Finally get the Connection ID and update its IP
     					$conID = $game->getConId($this->digestData['userID']);

     					return $game->setVmIp($conID, $vmIP);
					}
        		}
        		else{
        			throw new Exception("Error in state request, must be on or off", 1);
        		}
	        }
        	else{
        		throw new Exception("Game ID must be greater than 0 and not Null", 1);
        	}
	    }

	    /**
	     * Processes a POST request for the resource "game": /api/game/$id/clone
	     * 1. Makes the digest for user authentication with his signature
	     * 2. Retrieves all needed data for creating stuff and checks if we have all of it
	     * 3. Creates a new Game in the DB, associating it with the User
	     * 4. Creates the connection to this new game
	     * 5. Creates all the connection parameters for this connection
	     * 6. Fixes the correct connection permissions for the user
	     * 7. Executes the shellScript and clones the VM for the User
	     * @return string The ShellScript output of the VM clone
	     * @throws mixed
	     */
	    private function processPOST(){

	    	//fill the digest data sent in the headers
     		$this->fillDigestData($this->method);

     		//Make the digest of the signature sent and the real signature
			if(!$this->makeDigest($this->digestData['signature'],$this->digestData['userID'])
				|| !$this->makeTimestampDigest($this->digestData['userID'],$this->digestData['timestamp'])){
				throw new Exception("Error in the Digest. Signature or userID are wrong", 1);
     		}

	    	if($this->id !== null){
	    		//QUITAR CONDICION DE STATE
        		//Clone VM
         		if($this->state !== null && ($this->state == "clone" || $this->state == "test")){

         			//Fix the correct state name for passing it to the script
					$this->switchCorrectState();

         			$game = new GameCtrl();
         			$user = new UserCtrl();

         			//test section
         			// $headers = getallheaders();
         			// $gameID = $headers['gameID'];
         			// $userId = $this->digestData['userID'];
         			// $conName = "CON_USER_" . $userId . "_GAME_" . $gameID;
         			// $conID = 2;
         			// $conParams = array(
         			// 	'hostname' => 'cgvmhost',
         			// 	'port' => '3389',
         			// 	'username' => 'cgplayer',
         			// 	'password' => 'cgplayerpass',
         			// 	'width' => '800',
         			// 	'height' => '600',
         			// 	'security' => 'nla',
         			// 	'ignore-cert' => true,
         			// 	'remote-app' => 'remote-app'
         			// 	);
         			// $conID = $game->addConnection($conName);
         			// $result = $game->addGame(1,$userId,1);
         			// //$result = $game->addParametersToConnection($conID,$conParams);
         			// return $result;
         			//end of test section

         			//retrieve data before inserting new game
         			$userId = $this->digestData['userID'];
         			$userNameOfGame = $user->getName($this->digestData['userID']);
         			$vmTargetToclone = $game->getVMName($this->id);
         			$internalName = $game->getInternalName($this->id);
         			$vmName = 'VM_USER_'.$userId.'_'.$internalName;
         			$groupName = $game->getVMGroup($this->id);

         			//check if all necessary data is correct (almost if is not empty!!!)
         			if(empty($userId) || empty($userNameOfGame) || empty($vmTargetToclone)
         				|| empty($internalName)){
         				throw new Exception("Error Retrieving data before insert new game in clone process", 1);
         			}

         			//add a new game, then we query it with its ID
         			//adding new game creates also the association with the user
         			$gameID = $game->addGame($this->id,$userId,$vmName);

         			//Add a new connection to the new Game
         			$conName = "CON_USER_" . $userId . "_GAME_" . $gameID;
         			$conGroupId = $game->getConGroup($userId);
         			$conID = $game->addConnection($conName,$conGroupId);
         			//$path = $game->getAppPath($gameID);

         			//Now add all the parameters to the new connection
         			$conParams = array(
         				'hostname' => 'cgvmhost',
         				'port' => '3389',
         				'username' => 'cgplayer',
         				'password' => 'cgplayerpass',
         				'width' => '800',
         				'height' => '600',
         				'color-depth' => 16
         				);

         			$game->addParametersToConnection($conID,$conParams);

					//prepare args for the shell script
					// $1 -> commandArg
					// $2 -> vmName
					// $3 -> vmName to clone
					// $4 -> VM group name
					// $5 -> username
					$args = array(
						'commandArg' => $this->state,
						'vmName' => $vmName,
						'vmTargetToclone' => $vmTargetToclone,
						'groupName' => $groupName,
						'userName' => $userNameOfGame
					);

					//finnally add connection permissions
					$result = $game->addConPermissions($userId, $conID, $conGroupId);

					//Exec the ShellScript for clonning the VM
        			$shell = new Shell();
     				return $shell->execCommand($args);
         		}
         		else{
         			throw new Exception("Error in state request, must be clone", 1);
         		}
	        }
        	else{
        		throw new Exception("Game ID must be greater than 0 and not Null", 1);
        	}
	    }

	    /**
	     * Processes a GET request for the resource "game": /api/game/?id/?status
	     * 1. Makes the digest for authenticating the user with his signature
	     * 2. If an User ID is sent and status sent = status, returns the VM status
	     * 3. If an User ID is sent and no status sent, returns all user Games
	     * 4. If no User ID is sent, returns all Games that aren't templates
	     * @return mixed
	     * @throws mixed
	     */
	    private function processGameGET(){

	    	//fill the digest data sent in the headers
     		$this->fillDigestData($this->method);
     		//Make the digest of the signature sent and the real signature
			if(!$this->makeDigest($this->digestData['signature'],$this->digestData['userID'])
				|| !$this->makeTimestampDigest($this->digestData['userID'],$this->digestData['timestamp'])){
				throw new Exception("Error in the Digest. Signature or userID are wrong", 1);
     		}
	    	if($this->id !== null){
     			//Return the Game State (VM State)
     			if($this->state !== null && $this->state == "status"){
     				$shell = new Shell();
     				return $shell->execCommand($args);

     			}	//list all User Games
     			else if($this->state === null){
     				$game = new GameCTRL();
     				return $game->getAllGames($this->id);
     			}
     			else{
     				throw new Exception("Invalid State argument in the GET request", 1);
     			}
     		}
     		//get game from connection
     		else if($this->verb != ""){
     			$conName = $this->verb;
     			//list All Games in Database
     			$game = new GameCTRL();
     			return $game->getGameFromConnection($conName);
     		}
     		else{
     			//list All Games in Database
     			$game = new GameCTRL();
     			return $game->getAllGames();
     		}
	    }

	    /**
	     * Process GET requests for resource User, with verb sent "login"
	     * 1. Makes the digest authentication with the User signature in Base64
	     * 2. Generates a new User Api Key and stores it and sends it to the User
	     * @return array data sent back to the User
	     * @throws mixed
	     */
	    private function processLoginGET(){

	    	$this->checkNewUserDigestData($this->method);

    		$password = $_GET['password'];
			$name = $_GET['username'];
			$timestamp = $_GET['timestamp'];

			//decode base64 timestamp
			$timestamp = base64_decode($timestamp);

			$date = new DateTime();
        	$current_timestamp = $date->getTimestamp();

        	//digest Timestamp between 5 minutes
        	$variance = $current_timestamp - $timestamp;
        	if(!($variance >= 300) && !($variance <= 300))
        		throw new Exception("Error. Timestamp expired", 1);

			$user = new UserCtrl();
			$this->id = $user->getID($name);
			$success = $user->checkLogin($this->id,$password);

			//check success
			if(!$success){
				throw new Exception("Login error. Username/Password is incorrect", 1);
   			}

			//generate a new api_key for this session, store it and send it to the user
			$API_KEY = sha1(microtime(true).mt_rand(10000,90000));

			//store the temporally session user key
			$success = $user->storeApiKey($this->id,$API_KEY);

			$data = array(	'key' => $API_KEY,
							'id' => $this->id,
							'name' => $name
							);
			return $data;
	    }

	    /**
	     * Process GET requests for resource User, with verb sent "logout"
	     * 1. Makes the digest for authentication with user signature hashed with his api key
	     * 2. Stores a ney Api Key invalidating the last one
	     * @return bool 1 or true if success
	     * @throws Exception if wrong digest
	     */
	    private function processLogoutGet(){
	    	$user = new UserCtrl();

    		//fill the digest data sent in the headers
     		$this->fillDigestData($this->method);

     		//Make the digest of the signature sent and the real signature
			if(!$this->makeDigest($this->digestData['signature'],$this->digestData['userID'])
				|| !$this->makeTimestampDigest($this->digestData['userID'],$this->digestData['timestamp'])){
				throw new Exception("Error in the Digest. Signature or userID are wrong", 1);
     		}

     		//generate a new api_key for this session, store it and send it to the user
    		$API_KEY = sha1(microtime(true).mt_rand(10000,90000));

    		//store the temporally session user key
			$success = $user->storeApiKey($this->digestData['userID'],$API_KEY);

			return $success;
	    }

	    /**
	     * Creates a new User in the DB:
	     * 1. Makes the digest for authentication with the User signature in Base64
	     * 2. Stores the User and a new Api Key in the DB
	     * 3. Returns the new User ID in the request
	     * @return int The new User ID
	     * @throws mixed
	     */
	    private function processRegisterPOST(){

	    	//check if all data is sent
	    	$this->checkNewUserDigestData($this->method);

	    	$user = new UserCtrl();

    		$username = $_POST['username'];
    		$password = $_POST['password'];
    		$timestamp = $_POST['timestamp'];

			//decode base64 timestamp
			$timestamp = base64_decode($timestamp);
			//decode username and password
			$password = base64_decode($password);
			$username = base64_decode($username);

			$date = new DateTime();
        	$current_timestamp = $date->getTimestamp();

        	//digest Timestamp between 5 minutes
        	$variance = $current_timestamp - $timestamp;
        	//return $variance;
        	if($variance >= 300 || $variance <= -300)
        		throw new Exception("Error. Timestamp expired", 1);

        	//now check if the user already exists
        	if($user->existsUser($username)){
        		throw new Exception("Error. User already exists", 1);
        	}

     		//generate a new api_key for this session, store it and send it to the user
    		$API_KEY = sha1(microtime(true).mt_rand(10000,90000));

    		$userData = array(
    			'apiKey' => $API_KEY,
    			'username' => $username,
    			'password' => $password
    			);

    		//Add the user and retrieve the id just created
			$success = $user->addUser($userData);
			$newUserID = array('id' => $success);

			return $newUserID;
	    }

	    /********
	    *	ENDPOINTS
	    *	*****/

	    /**
	     * Endpoint that listens for request to the game API and redirect them to the
	     * correct method.
	     * @param  string $args Arguments recieved and processed by the supper API
	     * @return mixed
	     * @throws mixed
	     */
	     protected function game($args = null){
	     	$this->sanitizeArgs($args);

	     	switch ($this->method) {
	     		case 'GET':
	     			$result = $this->processGameGET();
	     			break;
	     		case 'POST':
	     			$result = $this->processPOST();
	     			break;
	     		case 'PUT':
	     			$result = $this->processPUT();
	     			break;
	     	}

	     	return $result;
	     }

	     /**
	     * Endpoint that listens for request to the user API and redirect them to the correct
	     * Method.
	     * @param  string $args Arguments recieved and processed by the supper API
	     * @return mixed
	     * @throws mixed
	     */
	     protected function user($args = null){

	     	$this->sanitizeArgs($args);

	     	switch ($this->method) {
	     		case 'GET':
		     		if($this->verb !== null && $this->verb != "" && $this->verb == "logout"){
		     			$result = $this->processLogoutGET();
		     		}
		     		else if($this->verb !== null && $this->verb != "" && $this->verb == "login"){
		     			$result = $this->processLoginGET();
		     		}
		     		else{
		     			throw new Exception("Error in the Request Verb", 1);
		     		}
		     		break;
		     	case 'POST':
		     		if($this->verb !== null && $this->verb != "" && $this->verb == "new"){
		     			$result = $this->processRegisterPOST();
		     		}
		     		else{
		     			throw new Exception("Error in the Request Verb", 1);
		     		}
	     			break;
	     	}

	     	return $result;
	     }
	 }
 ?>