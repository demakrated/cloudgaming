#/bin/sh

# $1 funcion a ejecutar
# $2 nombre de la VM clonada a usar
# $3 nombre de la VM objetivo
# $4 nombre del grupo al que peretenecera la VM clonada
# $5 nombre del usuario al que peretenecera la VM clonada

funcToExec=$1
vmName=$2
vmTargetToclone=$3
groupName=$4
path='/Volumes/NTFSLocal/Mac/proyecto/VMS/Users/'
templatepath='/Volumes/NTFSLocal/Mac/proyecto/VMS/Templates/'
userName=$5

# switch de la funcion a ejecutar
function execFunction(){
	if [[ "$funcToExec" == "clonevm" ]]; then
		#statements
		clonevm
	elif [[ "$funcToExec" == "startvm" ]]; then
		#statements
		startvm
	elif [[ "$funcToExec" == "stopvm" ]]; then
		#statements
		stopvm
	elif [[ "$funcToExec" == "getvmstatus" ]]; then
		#statements
		getvmstatus
	elif [[ "$funcToExec" == "getvmip" ]]; then
		#statements
		getvmip
	fi
}

# funciones

# clone VM
function clonevm(){
	echo "Cloning VM..."
	mkdir $path$userName
	VBoxManage clonevm $vmTargetToclone --name $vmName --basefolder $path$userName --options keepdisknames --mode machine --register
 	VBoxManage modifyvm $vmName --groups $groupName
}

# start VM
function startvm(){
	VBoxManage startvm $vmName
}

# stop VM
function stopvm(){
	VBoxManage controlvm $vmName acpipowerbutton
	echo "Shutting down VM Machine..."
	sleep 15
	if [[ $(getvmstatus) == "on" ]]; then
		echo "Still runnig, forcing shutdown..."
		VBoxManage controlvm $vmName poweroff
	fi
	echo "done."
}

function getvmstatus(){
	status=$(VBoxManage showvminfo $vmName|grep State|awk '{print $2}')
	if [[ "$status" == "running" ]]; then
		echo "on"
	else
		echo "off"
	fi
}

function getvmip(){
	result=$(VBoxManage guestproperty get $vmName "/VirtualBox/GuestInfo/Net/0/V4/IP"|awk '{print $2}')
	echo $result
}

# invoke target function
execFunction