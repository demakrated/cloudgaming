<?php

	include_once("Db.class.php");

	/**
	 * DAO Layer User Class
	 *
	 * @author Author: Daniel Pedrajas Van de Velde
	 * @version 0.1
	 **/
	class User
	{

		/**
		 * Last DAO for querying Database
		 * @var Db.class
		 */
		private $DB;

		/**
		 * Last ID inserted in the actual transaction
		 * @var int
		 */
		private $lastInsertId;

		/**
		 * Default constructor, initializes DB DAO object
		 * @return type
		 */
		function __construct()
		{
			$this->DB = new DB();
		}

		/**
		 * Returns the User VMGroup name
		 * @param string $id User ID
		 * @return array VMGroup Name
		 * @throws Exception if User ID is null
		 */
		public function getVmgroup($id){
			if($id !== null){
				$query = "select * from vm_group as gr, cg_user as u where cg_user
						where gr.id = u.vm_group_id AND u.user_id = :id";
				$params = array('id' => $id);
				$result = $this->DB->query($query,$params);

				return $result;
			}
			else{
				throw new Exception("User Id must be not null", 1);
			}
		}

		/**
		 * Returns a data row of the User with id = $id
		 * @param  string $id User id to fetch
		 * @return array User data
		 * @throws Exception if User ID is null
		 */
		public function getUser($id = null){
			if($id !== null){
				$query = "select * from cg_user as u where u.user_id = :id";
				$params = array('id' => $id );
				$result = $this->DB->query($query,$params);

				return $result;
			}
			else{
				throw new Exception("User Id must be not null", 1);
			}
		}

		/**
		 * Associates an User and a Game in the DB
		 * @param  string $gameId Game ID to associate
		 * @param  string $userID User ID to associate
		 * @return bool         true if no errors
		 * @throws Exception if User ID or Game ID is null
		 */
		public function associateUserGame($gameId,$userID){
			if($gameId !== null && $userID !== null){
				$query = "insert into has_game values (:uid,:gid)";
				$params = array('gid' => $gameId, 'uid' => $userID);
				$result = $this->DB->query($query,$params);
				if($result != true){
					throw new Exception("An error ocurred. User Id and Game Id must be not null", 1);
				}

				return $result;
			}
			else{
				throw new Exception("User Id and Game Id must be not null", 1);
			}
		}

		/**
		 * Stores the new Hashed User Api Key and resets the connection because this query
		 * is executed afted a call to a stored procedure qhich leaves the DB cursor opened
		 * @param  int $id The User ID
		 * @param  string $key The new User Api Key
		 * @return int The number of rows affected
		 * @throws Exception if User ID or ApiKey is null
		 */
		public function storeApiKey($id = null,$key = null){
			if($key !== null && $id !== null){
				$query = "UPDATE cg_user SET api_key = :key WHERE cg_user.user_id = :id";
				$params = array('key' => $key, 'id' => $id);
				$resetConn = true;
				$result = $this->DB->query($query,$params,$resetConn);

				return $result;
			}
			else{
				throw new Exception("User Id and Game Id must be not null", 1);
			}
		}

		/**
		 * Returns the Hashed User Api Key
		 * @param  int $id The User ID
		 * @return string The User Api Key
		 * @throws Exception if User ID is null
		 */
		public function getApiKey($id = null){
			if($id !== null){
				$query = "SELECT u.api_key FROM cg_user as u WHERE u.user_id = :id";
				$params = array('id' => $id);
				$resetConn = false;
				$result = $this->DB->single($query,$params,$resetConn);

				return $result;
			}
			else{
				throw new Exception("User Id and Game Id must be not null", 1);
			}
		}

		/**
		 * Returns last inserted ID in an INSERT or UPDATE query transaction
		 * @return int last inserted ID
		 */
		public function getLastInsertId(){
			return $this->lastInsertId;
		}

		/**
		 * Checks if the User Name and the Passoword are correct in the database.
		 * We need to call a stored procedure, checkLogin, because for compare the password
		 * we need to hash it with the User Salt in the DB
		 *
		 * @param int $userID The User ID
		 * @param string $password The User Password
		 * @return bool true if the data is correct
		 * @throws Exception if User ID or User Password is null
		 */
		public function checkLogin($userID = null, $password = null){
			if($userID !== null && $password !== null){
				 $query = "CALL checkLogin(:id,:pass)";
				 $params = array('id' => (integer)$userID, 'pass' => $password);
				 $result = $this->DB->single($query,$params);

				return $result;
			}
			else{
				throw new Exception("User Id and Password must be not null", 1);
			}
		}

		/**
		 * Creates a new User when is registered. We need to call a stored procedure,
		 * addUser, because we need to generate the User unique Salt and Hash his password
		 * with this Salt. Also a trigger is fired for copying the user ID into our cg_user table
		 *
		 * @param array $userData The User Data
		 * @return int the New User ID
		 * @throws Exception if User Data is null
		 */
		public function addUser($userData = null){
			if($userData['username'] !== null && $userData['password'] !== null
				&& $userData['apiKey'] !== null){
				//First we add the guacamole user using the stored procedure
				//Then the guacamole_user trigger will add the user in cg_user
				 $query = "CALL addUser(:username,:password)";
				 $params = array('username' => $userData['username'], 'password' => $userData['password']);
				 $result = $this->DB->single($query,$params);
				 $this->lastInsertId = $result;

				return $this->lastInsertId;
			}
			else{
				throw new Exception("User Data and VMGroup Data must be not null", 1);
			}
		}

		/**
		 * Returns the User Name
		 * @param  int $userID The User ID
		 * @return int The User ID
		 * @throws Exception if User ID is null
		 */
		public function getName($userID = null){
			if($userID !== null){
				 $query = "SELECT u.username FROM guacamole_user as u
				 	WHERE u.user_id = :id";
				 $params = array('id' => $userID);
				 $result = $this->DB->single($query,$params);

				return $result;
			}
			else{
				throw new Exception("User Id must be not null", 1);
			}
		}

		/**
		 * Returns the User ID by his given name
		 * @param  string $userName The User name
		 * @return int The User ID
		 * @throws Exception if User name is null
		 */
		public function getID($userName = null){
			if($userName !== null){
				 $query = "select u.user_id from guacamole_user as u where u.username = :name";
				 $params = array('name' => $userName);
				 $result = $this->DB->single($query,$params);

				return $result;
			}
			else{
				throw new Exception("User Name must be not null", 1);
			}
		}

		public function existsUser($userName = null){
			if($userName !== null){
				 $query = "SELECT EXISTS (SELECT u.username FROM guacamole_user as u WHERE u.username = :name)";
				 $params = array('name' => $userName);
				 $result = $this->DB->single($query,$params);

				return $result;
			}
			else{
				throw new Exception("User Name must be not null", 1);
			}
		}
	}
 ?>