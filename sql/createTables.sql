
use cloudgaming;

CREATE TABLE `guacamole_connection_group` (

  `connection_group_id`   int(11)      NOT NULL AUTO_INCREMENT,
  `parent_id`             int(11),
  `connection_group_name` varchar(128) NOT NULL,
  `type`                  enum('ORGANIZATIONAL',
                               'BALANCING') NOT NULL DEFAULT 'ORGANIZATIONAL',

  PRIMARY KEY (`connection_group_id`),
  UNIQUE KEY `connection_group_name_parent` (`connection_group_name`, `parent_id`),

  CONSTRAINT `guacamole_connection_group_ibfk_1`
    FOREIGN KEY (`parent_id`)
    REFERENCES `guacamole_connection_group` (`connection_group_id`) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `guacamole_connection` (

  connection_id       int(11)      NOT NULL AUTO_INCREMENT,
  connection_name     varchar(128) NOT NULL,
  `parent_id`           int(11),
  `protocol`            varchar(32)  NOT NULL,
  
  PRIMARY KEY (`connection_id`),
  UNIQUE KEY `connection_name_parent` (`connection_name`, `parent_id`),

  CONSTRAINT `guacamole_connection_ibfk_1`
    FOREIGN KEY (`parent_id`)
    REFERENCES `guacamole_connection_group` (`connection_group_id`) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table of users. Each user has a unique username and a hashed password
-- with corresponding salt.
--
CREATE TABLE `guacamole_user` (

  `user_id`       int(11)      NOT NULL AUTO_INCREMENT,
  `username`      varchar(128) NOT NULL,
  `password_hash` binary(32)   NOT NULL,
  `password_salt` binary(32)   NOT NULL,

  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table of connection group permissions. Each group permission grants a user
-- specific access to a connection group.
--
CREATE TABLE `guacamole_connection_group_permission` (

  `user_id`             int(11) NOT NULL,
  `connection_group_id` int(11) NOT NULL,
  `permission`          enum('READ',
                             'UPDATE',
                             'DELETE',
                             'ADMINISTER') NOT NULL,

  PRIMARY KEY (`user_id`,`connection_group_id`,`permission`),

  CONSTRAINT `guacamole_connection_group_permission_ibfk_1`
    FOREIGN KEY (`connection_group_id`)
    REFERENCES `guacamole_connection_group` (`connection_group_id`) ON DELETE CASCADE,

  CONSTRAINT `guacamole_connection_group_permission_ibfk_2`
    FOREIGN KEY (`user_id`)
    REFERENCES `guacamole_user` (`user_id`) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;


create table vm_group(
  id int(11) auto_increment,
  name varchar(50),
  constraint pk_vmgr primary key (id)
 )ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table of connection history records. Each record defines a specific user's
-- session, including the connection used, the start time, and the end time
-- (if any).
--
CREATE TABLE `guacamole_connection_history` (

  `history_id`    int(11)  NOT NULL AUTO_INCREMENT,
  `user_id`       int(11)  NOT NULL,
  `connection_id` int(11)  NOT NULL,
  `start_date`    datetime NOT NULL,
  `end_date`      datetime DEFAULT NULL,

  PRIMARY KEY (`history_id`),
  KEY `user_id` (`user_id`),
  KEY `connection_id` (`connection_id`),

  CONSTRAINT `guacamole_connection_history_ibfk_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `guacamole_user` (`user_id`) ON DELETE CASCADE,

  CONSTRAINT `guacamole_connection_history_ibfk_2`
    FOREIGN KEY (`connection_id`)
    REFERENCES `guacamole_connection` (`connection_id`) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table of connection parameters. Each parameter is simply a name/value pair
-- associated with a connection.
--
CREATE TABLE `guacamole_connection_parameter` (

  `connection_id`   int(11)       NOT NULL,
  `parameter_name`  varchar(128)  NOT NULL,
  `parameter_value` varchar(4096) NOT NULL,

  PRIMARY KEY (`connection_id`,`parameter_name`),

  CONSTRAINT `guacamole_connection_parameter_ibfk_1`
    FOREIGN KEY (`connection_id`)
    REFERENCES `guacamole_connection` (`connection_id`) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table of connection permissions. Each connection permission grants a user
-- specific access to a connection.
--
CREATE TABLE `guacamole_connection_permission` (

  `user_id`       int(11) NOT NULL,
  `connection_id` int(11) NOT NULL,
  `permission`    enum('READ',
                       'UPDATE',
                       'DELETE',
                       'ADMINISTER') NOT NULL,

  PRIMARY KEY (`user_id`,`connection_id`,`permission`),

  CONSTRAINT `guacamole_connection_permission_ibfk_1`
    FOREIGN KEY (`connection_id`)
    REFERENCES `guacamole_connection` (`connection_id`) ON DELETE CASCADE,

  CONSTRAINT `guacamole_connection_permission_ibfk_2`
    FOREIGN KEY (`user_id`)
    REFERENCES `guacamole_user` (`user_id`) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table of system permissions. Each system permission grants a user a
-- system-level privilege of some kind.
--
CREATE TABLE `guacamole_system_permission` (

  `user_id`    int(11) NOT NULL,
  `permission` enum('CREATE_CONNECTION',
		    'CREATE_CONNECTION_GROUP',
                    'CREATE_USER',
                    'ADMINISTER') NOT NULL,

  PRIMARY KEY (`user_id`,`permission`),

  CONSTRAINT `guacamole_system_permission_ibfk_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `guacamole_user` (`user_id`) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Table of users_custom for the wep application. Each user has a unique username and a hashed password
-- with corresponding salt.
--
CREATE TABLE `cg_user` (

  `user_id`       int(11)      NOT NULL AUTO_INCREMENT,
  vm_group_id int(11) not null unique,
	api_key varchar(150),
  constraint fk_user_guacauser foreign key (user_id) references guacamole_user(user_id)
    on delete cascade,
  constraint fk_user_vmgroup foreign key (vm_group_id) references vm_group(id)
    on delete cascade,

  PRIMARY KEY (`user_id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table of user permissions. Each user permission grants a user access to
-- another user (the "affected" user) for a specific type of operation.
--
CREATE TABLE `guacamole_user_permission` (

  `user_id`          int(11) NOT NULL,
  `affected_user_id` int(11) NOT NULL,
  `permission`       enum('READ',
                          'UPDATE',
                          'DELETE',
                          'ADMINISTER') NOT NULL,

  PRIMARY KEY (`user_id`,`affected_user_id`,`permission`),

  CONSTRAINT `guacamole_user_permission_ibfk_1`
    FOREIGN KEY (`affected_user_id`)
    REFERENCES `guacamole_user` (`user_id`) ON DELETE CASCADE,

  CONSTRAINT `guacamole_user_permission_ibfk_2`
    FOREIGN KEY (`user_id`)
    REFERENCES `guacamole_user` (`user_id`) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;



create table game(
  id int(11) auto_increment,
  name varchar(50) not null,
  path varchar(150) not null,
  isTemplate bool not null,
  image varchar(150),
  internalName varchar(50) not null,
  genero varchar(50),
  precio float,
  constraint pk_game primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table has_game(
  user_id int(11),
  game_id int(11),
  constraint pk_hasGame primary key (user_id,game_id),
  constraint fk_hgam_user foreign key (user_id) references guacamole_user(user_id) ON DELETE CASCADE,
  constraint fk_hgam_game foreign key (game_id) references game(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table vm(
  id int(11) auto_increment,
  name varchar(50),
  game_id int(11) not null,
  vm_group_id int(11) not null,
  constraint pk_vm primary key (id),
  constraint fk_vm_game foreign key (game_id) references game(id) ON DELETE CASCADE,
  constraint fk_vm_vmgr foreign key (vm_group_id) references vm_group(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- triger para insertar en mi tabla de usuario cuando se cree uno desde la interfaz o API
DELIMITER $$
CREATE TRIGGER ins_user AFTER INSERT ON guacamole_user
FOR EACH ROW BEGIN
	INSERT INTO vm_group (name) values (CONCAT('/VMGroup_',NEW.user_id,'_',NEW.username));
    INSERT INTO cg_user (user_id, vm_group_id) values(NEW.user_id,
		(SELECT MAX(id) FROM vm_group));

	-- Create connection group
	INSERT INTO guacamole_connection_group (connection_group_name, type)
		VALUES (CONCAT('CONGROUP_USER_',NEW.user_id), 'ORGANIZATIONAL');
	
  END $$

-- stored procedure that checks user login
DELIMITER $$
CREATE PROCEDURE `checkLogin`(IN id int(11), passw VARCHAR(50))
BEGIN
        DECLARE pass blob;
		DECLARE originalPass blob;
		DECLARE salt blob;
		DECLARE result bool;

		SET salt := (select u.password_salt from cloudgaming.guacamole_user as u where u.user_id = id);
		SET originalPass := (select u.password_hash from cloudgaming.guacamole_user as u where u.user_id = id);
		SET pass := (UNHEX(SHA2(CONCAT(passw, HEX(salt)), 256)));
		SET result := (select pass = originalPass);
 
		select result;
    END$$
DELIMITER ;

-- stored procedure for add an user_resources and returns the last id inserted
DELIMITER $$
CREATE PROCEDURE `addUser`(IN username VARCHAR(50), pass VARCHAR(50))
	BEGIN

		DECLARE salt blob;

		-- Generate salt
		SET @salt = UNHEX(SHA2(UUID(), 256));

		-- Create user and hash password with salt
		INSERT INTO guacamole_user (username, password_salt, password_hash)
			VALUES (username, @salt, UNHEX(SHA2(CONCAT(pass, HEX(@salt)), 256)));

		SELECT MAX(user_id) FROM guacamole_user;

    END$$

-- stored procedure for a connection to the user and returns the last id inserted
DELIMITER $$
CREATE PROCEDURE `addCon`(IN conName VARCHAR(50), conGroupId VARCHAR(50))
	BEGIN

		-- Create connection
		INSERT INTO guacamole_connection (connection_name, protocol, parent_id) 
			VALUES (conName, 'rdp', conGroupId);

		SELECT LAST_INSERT_ID();
    END$$