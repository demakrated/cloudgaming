
$(document).ready(function() {

	

	
	//Query all Games
	$('#stopGame').click(function(event) {
		if(!checkSessionAndClear()){
			return;
		}

		var gameID = $('#inputGameID').val();
		var data = $.cookie('name') + $.cookie('id');
		var signature = stampSignature(data);

		var data = signature + ',' + $.cookie('id') + ','  + stampTimestampSignature();

		$.ajax({
			url: gamesURL + gameID + '/off',
			type: 'PUT',
			data: data
		})
		.done(function(data) {
			console.log("success");
			console.log(data);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	});

	//Query all Games
	$('#startGame').click(function(event) {
		if(!checkSessionAndClear()){
			return;
		}
		var gameID = '73';
		var data = $.cookie('name') + $.cookie('id');
		var signature = stampSignature(data);

		var data = signature + ',' + $.cookie('id') + ','  + stampTimestampSignature();

		$.ajax({
			url: gamesURL + gameID + '/on',
			type: 'PUT',
			data: data
		})
		.done(function(data) {
			console.log("success");
			console.log(data);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	});

	//Check cookies
	if(!checkSessionAndClear()){
		$('#logout-panel').hide();
	}
	else{
		//add elements if we are loged in
		getAllGames();
		getUserGames();
		fillUserData();
		fixUserGameList();
		setCorrectUserGames();
	}
	//fix on reload userGames list
	fixUserGameList();
	//attach some listeners
	attachListeners();


});

//JS

//variables
var loginURL = 'http://192.168.1.103/api/user/login/';
var logoutURL = 'http://192.168.1.103/api/user/logout/';
var resgisterURL = 'http://192.168.1.103/api/user/new/';
var gamesURL = 'http://192.168.1.103/api/game/';
var userGames = {};
var allGames = {};

//FUNCTIONS

//Attach some listeners
function attachListeners(){
	//Login, retrieve data and set cookie that expires in 1 day
	$('#login').on('click',function(event) {
		userLogin();
	});
	//Logout, destroy cookie
	$("#logout").on('click',function(event) {
		userLogout();
	});

	//Register new user
	$('#register-button').on('click',function(event) {
		registerNewUser();
	});

	$('#register-popup').on('click',function(event) {
		registerPopUp('#dialog');
	});
}

function registerPopUp(popupSelector){
	$(popupSelector).dialog({
		resizable: false,
	 	closeOnEscape: true,
	 	hide: "fadeOut",
	 	show: { duration: 800 }
	});

	//$('#popup-fog').fadeIn(800);
	$(popupSelector).dialog('open');
}

//Fills welcome message
function fillUserData(){
	$('#loged-user').html('Bienvenido, ' + $.cookie('name'))
}

//Disable autocompletion for the forms
function disableAutocomplete(){
	$('#username').attr("autocomplete","off");
	$('#password').attr("autocomplete","off");
	$('#username-reg').attr("autocomplete","off");
	$('#password-reg').attr("autocomplete","off");
}

//Fixes the correcto view of user Connection List as games
function fixUserGameList(){
	$('.group').addClass('expanded');
	$('.caption:first').remove();
}


/***************************************
* User related functions
* **************************************/

//Register
function registerNewUser(){
	var username = $('#username-reg').val();
	var password = $('#password-reg').val();
	var passwordMatch = $('#password-reg2').val();

	if(!checkUserAndPass(username,password,passwordMatch)){
		return;
	}

	var d = new Date();
	//encode base64 timestamp because we havent the api key yet
    var timestamp = d.getTime()/1000;
    timestamp = utf8_to_b64(timestamp);

	$.ajax({
		url: resgisterURL,
		type: 'POST',
		data: {password: password, username: username, timestamp: timestamp}
	})
	.done(function(data) {
		if(data.hasOwnProperty("error")){
			console.log(data['error']);
			$('#password-match').animate({opacity:0},function(){
    			$(this).text("El usuario no está disponible")
        		.animate({opacity:1});
        	});
		}
		else if(!(data.hasOwnProperty("id"))){
			console.log("Alert, get response BUT NO USER ID provided in response.");
			return;
		}
		else{
			console.log("success");
			$("#dialog").dialog('close');
			$('#popup-fog').fadeOut();
		}
	})
	.fail(function(data) {
		console.log("error");
		console.log(data);
	})
	.always(function(data) {
		console.log("complete");
		console.log(data);
	});
}

//Login
function userLogin(){
	var d = new Date();
	//encode base64 timestamp because we havent the api key yet
    var timestamp = d.getTime()/1000;
    timestamp = utf8_to_b64(timestamp);

	var username = $('#username').val();
	var password = $('#password').val();

	$.ajax({
		url: loginURL,
		type: 'GET',
		data: {password: password, username: username, timestamp: timestamp}
	})
	.done(function(data) {
		console.log("success");
		console.log(data);
		if(!(data.hasOwnProperty("key"))){
			console.log("Alert, get response BUT NO KEY provided in response.");
			return;
		}
		if(!(data.hasOwnProperty("id"))){
			console.log("Alert, get response BUT NO USER ID provided in response.");
			return;
		}

		$.cookie('key',data['key'], { expires: 1 });
		$.cookie('id',data['id'], { expires: 1 });
		$.cookie('name',data['name'], { expires: 1 });

		//query games data
		getAllGames();
		getUserGames();
		fillUserData();
		fixUserGameList();
		setCorrectUserGames();

		$('#logout-panel').show();

	})
	.fail(function(data) {
		console.log("error");
		console.log(data);
	})
	.always(function(data) {
		console.log("complete");
	});
}

//Logout
function userLogout(){
	var data = $.cookie('name') + $.cookie('id');

	var signature = stampSignature(data);
	var timestamp = stampTimestampSignature();

	$.ajax({
		url: logoutURL,
		data: {signature: signature, userID: $.cookie('id'), timestamp: timestamp}
	})
	.done(function(data) {
		console.log("success");
		console.log(data);
		window.location.href = "logout";
	})
	.fail(function(data) {
		console.log(data);
		console.log("error");
	})
	.always(function() {
		logout();
		console.log("complete");
	});
}

//Check Username and password before proceed
function checkUserAndPass(username,pass1,pass2){
	if(username === null || username == ""){
		$('#password-match').animate({opacity:0},function(){
    		$(this).text("Introduzca un usuario")
        	.animate({opacity:1});
        });
		return false;
	}
	else if(pass1 === null || pass1 == ""){
		$('#password-match').animate({opacity:0},function(){
    		$(this).text("La contraseña no puede ser vacía")
        	.animate({opacity:1});
        });
		return false;
	}
	else if(pass1 != pass2){
		$('#password-match').animate({opacity:0},function(){
    		$(this).text("Las contraseñas no coinciden")
        	.animate({opacity:1});
        });
		return false;
	}

	return true;
}

//Destroys coockie data on logout
function logout() {
	$.removeCookie('key');
	$.removeCookie('id');
	$.removeCookie('name');
}

//Checks the session
function checkSessionAndClear(){
	if($.cookie('key') === undefined || $.cookie('key') === null ){
		console.log("Error. You are not loged");
		return false;
	}
	else{
		return true;
	}
}

//Sings data with User Api Key
function stampSignature(data){
	var hash = CryptoJS.HmacSHA256(data.toString(), $.cookie('key'));
	return hash.toString();
}

//Signs data in Bae64, used for register or login. We haven't any Key yet
function utf8_to_b64( str ) {
  	return window.btoa(unescape(encodeURIComponent( str )));
}

//Add a timestamp to the query signed with user Key or Base64 if we are not loged yet
function stampTimestampSignature(){
	var d = new Date();
	var timestamp = d.getTime()/1000;
	timestamp = timestamp.toString();
    return stampSignature(timestamp);
}

///////////////////////////////////////////////////

/************************
* Games related functions
* ***********************/

//Changes DOM to the correct view of the User Owned Games
function setCorrectUserGames(){
	var html = '';

	var data = $.cookie('name') + $.cookie('id');

	var signature = stampSignature(data);
	var timestamp = stampTimestampSignature();

	$.each($('.children'), function(index, val) {
		/* iterate through array or object */
		var node = $(this).find('.name');
		var conName = $(this).find('.name').html();
		console.log("node: "+node);

		//hide protocol image
		$(this).find('.protocol').hide();

		$.ajax({
			url: gamesURL + conName,
			data: {signature: signature, userID: $.cookie('id'), timestamp: timestamp}
		})
		.done(function(data) {
			console.log("success");
			var game = data[0];

			html = '<div class="game-connection" onclick="startGame(' + game.id + ');">' +
						'<span id="' + game.id + '"></span>' +
						'<img src="' + game.image + '" alt="' + game.name + '" />' +
						'<p>' +  game.name + '</p>' +
					'</div>';
			
			node.html(html);
			// node = $('div.connection.list-item');

			// html = '<button id="stopGame">Detener estación de juego</button>';
			//node.append(html);
		})
		.fail(function(data) {
			console.log(data);
			console.log("error");
		})
		.always(function() {
			//logout();
			console.log("complete");
		});
	});
	
}

//retrieve all User Games
function getUserGames(){
	if(!checkSessionAndClear()){
		return;
	}

	var data = $.cookie('name') + $.cookie('id');
	var signature = stampSignature(data);

	$.ajax({
		url: gamesURL + $.cookie('id'),
		data: {signature: signature, userID: $.cookie('id'), timestamp: stampTimestampSignature()}
	})
	.done(function(data) {
		console.log("success");
		console.log(data);
		userGames = data;
		//appendAllGames(data);
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

//retrieve all Games
function getAllGames(){
	if(!checkSessionAndClear()){
		return;
	}

	var data = $.cookie('name') + $.cookie('id');
	var signature = stampSignature(data);

	$.ajax({
		url: gamesURL,
		data: {signature: signature, userID: $.cookie('id'), timestamp: stampTimestampSignature()}
	})
	.done(function(data) {
		console.log("success");
		console.log(data);
		allGames = data;
		appendAllGames(data,'list-group');
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

function startGame(gameID){
	if(!checkSessionAndClear()){
		return;
	}

	var data = $.cookie('name') + $.cookie('id');
	var signature = stampSignature(data);

	var data = signature + ',' + $.cookie('id') + ','  + stampTimestampSignature();

	$.ajax({
		url: gamesURL + gameID + '/on',
		type: 'PUT',
		data: data
	})
	.done(function(data) {
		console.log("success");
		console.log(data);
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

//Append All Games to the user Page
function appendAllGames(data, node){
	/*
		properties of each game: id, name, path, isTemplate, image, price, genero
	 */
	for (var i = 0, l = data.length; i < l; i++) {
	    var game = data[i];
	    $('.'+node).append('<li style="list-style: none;"><div class="game" onclick="gameInfo('+game['id']+');"><span id="' + game['id']
	     	+ '"></span><img src="' + game['image'] + '" alt="' +game['name'] + '" />'
	     	+ '<p>' + game['name'] + '</p>' + '</div></li>');
	}
}

//Popup with the Game Info on click on it
function gameInfo(gameID){
	var myGame;
	$.each(allGames, function(index, val) {
		 /* iterate through array or object */
		 if(val.id == gameID){
		 	myGame = val;
		 }
	});
	var price = Number((myGame.precio).toFixed(2));

	var html ='';

	html += '<table>' +
                '<tr>' +
                    '<td><img src="' + myGame.image + '" alt="' + myGame.name + '" style="width: 100%;" /></td>' +
                '</tr>' +
                '<tr>' +
                    '<p>' + myGame.name + '</p>' +
                '</tr>' +
                '<tr>' +
                    '<p>Género: ' + myGame.genero + '</p>' +
                '</tr>' +
                '<tr>' +
                    '<p>Precio: ' + price + ' €</p>' +
                '</tr>' +
            '</table>' +
            '<button id="purchase-button" onclick="addGame(' + gameID + ');">Adquirir</button>';

    //add game html info
   	$('#game-dialog').html(html);

    //initialize game dialog
    $('#game-dialog').dialog({
			resizable: false,
		 	closeOnEscape: true,
		 	width: 400,
		 	hide: "fadeOut",
		 	show: { duration: 800 }
	});
	$('#game-dialog').dialog('open');

}

//Adds a game to the User profile
function addGame(gameID){
	if(!checkSessionAndClear()){
		return;
	}

	var data = $.cookie('name') + $.cookie('id');
	var signature = stampSignature(data);


	//show waiting wheel
	$('.modal').fadeIn('slow');

	$.ajax({
		url: gamesURL + gameID + '/clone',
		type: 'POST',
		data: {signature: signature, userID: $.cookie('id'), timestamp: stampTimestampSignature()}
	})
	.done(function(data) {
		console.log("success");
		console.log(data);
		location.reload();
	})
	.fail(function(data) {
		console.log("error");
		console.log(data);
		$('.modal').fadeOut('slow');
		alert(data);
	})
	.always(function() {
		console.log("complete");
	});
}