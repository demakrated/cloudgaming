<?php
	include_once("database/User.class.php");
	include_once("Controller.class.php");

	/**
	* UserController Class. Uses UserDAO layer for working with database.
	*
	* @author Author: Daniel Pedrajas Van de Velde
	* @version 0.1
	*/
	class UserCtrl extends Controller
	{
		/**
		 * UserDAO obect Layer for DB work
		 */
		private $userDAO;

		/**
		 * Constructs the controller.
		 * @param string $id if is not null initializes
		 * this instance as a copy with the User data that has id = $id
		 */
		function __construct($id = null)
		{
			$this->userDAO = new User();
			parent::__construct($id);
		}

		/**
		 * Initializes this instance as a copy of another
		 * @param  string $id the id of the database User referenced
		 * @return array row with User data
		 */
		protected function initAttrs($id){
			$result = $this->getUser($id);
			return $result;
		}

		/**
		 * @see class::Game::getUser
		 */
		public function getUser($id = null){
			$result = $this->userDAO->getUser($id);
			return $result;
		}

		/**
		 * @see class::Game::getID
		 */
		public function getID($userName = null){
			$result = $this->userDAO->getID($userName);
			return $result;
		}

		/**
		 * Makes a database association with an user and a given game. Must be fired
		 * when a User adds a game to his account
		 * @param string $gameId the Game ID
		 * @param string $userID the User ID
		 * @return bool. If no errors true
		 */
		public function associateUserGame($gameId = null,$userID = null){
			if(count($this->attrs) > 0 && $gameId !== null){
				$result = $this->userDAO->associateUserGame($gameId,(string)$this->attrs[0]["user_id"]);
				return $result;
			}
			else if($gameId !== null && $userID !== null){
				$result = $this->userDAO->associateUserGame($gameId,$userID);
				return $result;
			}
			else{
				throw new Exception("Error Processing Request", 1);
			}
		}

		/**
		 * @see class::Game::checkLogin
		 */
		public function checkLogin($id = null, $password = null){
			$result = $this->userDAO->checkLogin($id,$password);
			return $result;
		}

		/**
		 * @see class::Game::storeApiKey
		 */
		public function storeApiKey($id = null, $key = null){
			$result = $this->userDAO->storeApiKey($id,$key);
			return $result;
		}

		/**
		 * @see class::Game::getApiKey
		 */
		public function getApiKey($id = null){
			$result = $this->userDAO->getApiKey($id);
			return $result;
		}

		/**
		 * @see class::Game::getName
		 */
		public function getName($id = null){
			$result = $this->userDAO->getName($id);
			return $result;
		}

		/**
		 * @see class::Game::addUser
		 */
		public function addUser($userData){
			$result = $this->userDAO->addUser($userData);
			return $result;
		}

		public function existsUser($userName){
			$result = $this->userDAO->existsUser($userName);
			return $result;
		}
	}
 ?>