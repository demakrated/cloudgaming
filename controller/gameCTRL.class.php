<?php

	include_once("database/Game.class.php");
	include_once("Controller.class.php");

	/**
	 * GameController Class. Uses GameDAO layer for working with database.
	 *
	 * @author Author: Daniel Pedrajas Van de Velde
	 * @version 0.1
	 */
	class GameCtrl extends Controller
	{
		/**
		 * GameDAO obect Layer for DB work
		 * @var Game
		 */
		private $gameDAO;

		/**
		 * Constructs the controller.
		 * @param string $id if is not null initializes
		 * this instance as a copy with the Game data that has id = $id
		 */
		function __construct($id = null)
		{
			$this->gameDAO = new Game();
			parent::__construct($id);
		}

		/**
		 * Initializes this instance as a copy of another
		 * @param  string $id the id of the database Game referenced
		 * @return array:Game     row with Game data
		 */
		protected function initAttrs($id){
			$result = $this->getGame($id);
			return $result;
		}

		/**
		 * @see class::Game::getAllGames
		 */
		public function getAllGames($id = null){
			if( $id !== null){
				$result = $this->gameDAO->getAllGames($id);
			}
			else{
				$result = $this->gameDAO->getAllGames();
			}

			return $result;
		}

		/**
		 * @see class::Game::getGame
		 */
		public function getGame($id = null){
			$result = $this->gameDAO->getName($id);
			return $result;
		}

		/**
		 * @see class::Game::getGameName
		 */
		public function getGameName($id = null){
			$result = $this->gameDAO->getName($id);
			return $result;
		}

		/**
		 * @see class::Game::getVMName
		 */
		public function getVMName($gameId = null){
			$result = $this->gameDAO->getVMName($gameId);
			return $result;
		}

		/**
		 * @see class::Game::getVMGroup
		 */
		public function getVMGroup($gameId = null){
			$result = $this->gameDAO->getVMGroup($gameId);
			return $result;
		}

		/**
		 * @see class::Game::getUserNameOfGameWithoutVM
		 */
		public function getUserNameOfGameWithoutVM($gameId = null){
			$result = $this->gameDAO->getUserNameOfGameWithoutVM($gameId);
			return $result;
		}

		/**
		 * @see class::Game::getUserIdOfGame
		 */
		public function getUserIdOfGame($gameId = null){
			$result = $this->gameDAO->getUserIdOfGame($gameId);
			return $result;
		}

		/**
		 * @see class::Game::getInternalName
		 */
		public function getInternalName($gameId = null){
			$result = $this->gameDAO->getInternalName($gameId);
			return $result;
		}

		/**
		 * @see class::Game::addGameToVMGroup
		 */
		public function addGameToVMGroup($gameId = null, $userId = null){
			$result = $this->gameDAO->addGameToVMGroup($gameId, $userId);
			return $result;
		}

		/**
		 * @see class::Game::getAppPath
		 */
		public function getAppPath($gameId = null){
			$result = $this->gameDAO->getAppPath($gameId);
			return $result;
		}

		/**
		 * @see class::Game::addConnection
		 */
		public function addConnection($conName = null, $connGroupId = null){
			$result = $this->gameDAO->addConnection($conName,$connGroupId);
			return $result;
		}

		/**
		 * @see class::Game::addParameters
		 */
		public function addParametersToConnection($connID = null, $parameters = null){
			$result = $this->gameDAO->addParameters($connID,$parameters);
			return $result;
		}

		/**
		 * @see class::Game::getConGroup
		 */
		public function getConGroup($userId = null){
			$result = $this->gameDAO->getConGroup($userId);
			return $result;
		}

		/**
		 * @see class::Game::addConPermissions
		 */
		public function addConPermissions($UserId = null, $ConId = null, $ConGroupId = null){
			$result = $this->gameDAO->addConPermissions($UserId,$ConId,$ConGroupId);
			return $result;
		}

		/**
		 * @see class::Game::setVmIp
		 */
		public function setVmIp($ConId = null, $IP = null){
			$result = $this->gameDAO->setVmIp($ConId, $IP);
			return $result;
		}

		/**
		 * @see class::Game::getConId
		 */
		public function getConId($UserId = null){
			$result = $this->gameDAO->getConId($UserId);
			return $result;
		}

		/**
		 * @see class::Game::addGame
		 */
		public function addGame($gameId,$userId,$vmName){
			$result = $this->gameDAO->addGame($gameId,$userId,$vmName);
			return $result;
		}

		public function getGameFromConnection($conName){
			$result = $this->gameDAO->getGameFromConnection($conName);
			return $result;
		}

	}
 ?>