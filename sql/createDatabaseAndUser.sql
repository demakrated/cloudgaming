CREATE DATABASE cloudgaming;

CREATE USER 'cgadmin'@'%' IDENTIFIED BY 'cgadmin';

GRANT SELECT,INSERT,UPDATE,DELETE ON cloudgaming.* TO 'cgadmin'@'%';

GRANT ALTER ROUTINE, CREATE ROUTINE, EXECUTE ON cloudgaming.* TO 'cgadmin'@'%'; 

FLUSH PRIVILEGES;

