<?php

	include_once("Db.class.php");

	/**
	 * DAO Layer Game Class
	 *
	 * @author Author: Daniel Pedrajas Van de Velde
	 * @version 0.1
	 **/
	class Game
	{
		/**
		 * Last DAO for querying Database
		 * @var Db.class
		 */
		private $DB;

		/**
		 * Last ID inserted in the actual transaction
		 * @var int
		 */
		private $lastInsertId;

		/**
		 * Default constructor, initializes DB DAO object
		 */
		function __construct() {
			$this->DB = new DB();
		}

		/**
		 * Returns an array with all the Games. If $id is not null
		 * fetch all games data from the user with id = $id
		 *
		 * @param int the User ID for listing his games
		 * @return array Games list
		 **/
		public function getAllGames($userId = null)
		{
			if($userId !== null){
				$query = "select g.* from game as g, has_game as hg
					where g.id = hg.game_id AND g.isTemplate = false
					AND hg.user_id = :id";
					$params = array('id' => $userId );
					$result = $this->DB->query($query,$params);
			}
			else{
				$query = "select g.* from game as g where g.isTemplate = true;";
				$result = $this->DB->query($query);
			}

			return $result;
		}

		/**
		 * Returns the Game Name from a given $id
		 * @param int $id of target Game
		 * @return array Game Name
		 * @throws Exception if ID is null
		 */
		public function getName($id = null){
			if($id !== null){
				$query = "select g.name from game as g where g.id = :id";
				$params = array('id' => $id );
				$result = $this->DB->single($query,$params);
				return $result;
			}
			else{
				throw new Exception("Game Id must be not null", 1);
			}
		}

		/**
		 * Returns the VM Name with id = $id
		 * @param  int $id ID of VM target
		 * @return string Name of VM
		 * @throws Exception if ID is null
		 */
		public function getVMName($id = null){
			if($id !== null){
				$query = "select vm.name from vm where vm.game_id = :id";
				$params = array('id' => $id );
				$result = $this->DB->single($query,$params);
				return $result;
			}
			else{
				throw new Exception("Game Id must be not null", 1);
			}
		}

		/**
		 * Returns the VMGroup of the user with id = $id
		 * @param  int $id The ID of target User
		 * @return string The VMGroup name
		 * @throws Exception if ID is null
		 */
		public function getVMGroup($id = null){
			if($id !== null){
				$query = "SELECT gp.name FROM vm_group as gp, vm as v, game as g
					WHERE g.id = :id AND g.id = v.game_id AND v.vm_group_id = gp.id";
				$params = array('id' => $id );
				$result = $this->DB->single($query,$params);
				return $result;
			}
			else{
				throw new Exception("Game Id must be not null", 1);
			}
		}

		/**
		 * Returns the UserName whom hasn't a VM jet
		 * @param  int $gameID The ID of the target Game
		 * @return string The User name
		 * @throws Exception if GameID is null
		 */
		public function getUserNameOfGameWithoutVM($gameID = null){
			if($gameID !== null){
				$query = "SELECT gu.username FROM game as g, has_game as hg, cg_user as u,
					guacamole_user as gu
					WHERE g.id = :id AND g.id = hg.game_id AND hg.user_id = u.user_id AND
					u.user_id = gu.user_id";
				$params = array('id' => $gameID );
				$result = $this->DB->single($query,$params);
				return $result;
			}
			else{
				throw new Exception("Game ID must both be not null", 1);
			}
		}

		/**
		 * Returns the Internal Game Name
		 * @param  int $id The ID of target game
		 * @return array The Game name
		 * @throws Exception if GameID is null
		 */
		public function getInternalName($id = null){
			if($id !== null){
				$query = "select g.internalName from game as g where g.id = :id";
				$params = array('id' => $id );
				$result = $this->DB->single($query,$params);
				return $result;
			}
			else{
				throw new Exception("Game Id must be not null", 1);
			}
		}

		/**
		 * Returns a row containing the Game data
		 * @param int $id The ID of target Game
		 * @return array The Game data row
		 * @throws Exception if GameID is null
		 */
		public function getGame($id = null){
			if($id !== null){
				$query = "select * from game as g where g.id = :id";
				$params = array('id' => $id );
				$result = $this->DB->query($query,$params);
				return $result;
			}
			else{
				throw new Exception("Game Id must be not null", 1);
			}
		}

		/**
		 * Returns the VMGroup ID from a given User
		 * @param  int $userId The ID of target User
		 * @return int The VMGroup ID
		 * @throws Exception if UserID is null
		 */
		private function getVMGroupIDFromUser($userId = null){
			if($userId !== null){
				$query = "SELECT u.vm_group_id FROM cg_user as u WHERE u.user_id = :id";
				$params = array('id' => $userId);
				$result = $this->DB->single($query,$params);
				return $result;
			}
			else{
				throw new Exception("Game Id and User Id must be not null", 1);
			}
		}

		/**
		 * Creates the relationship in the DB between the VM and the Game that it hosts
		 * @param  int $gameId The Game ID
		 * @param  int $userId The User ID
		 * @param  string $vmName The VM name
		 * @return int number of rows affected
		 * @throws Exception if GameID, UserID or VMName is null
		 */
		private function associateGameToVM($gameId = null, $userId = null,$vmName = null){
			if($gameId !== null && $userId !== null && $vmName !== null){
				$vmGroupId = $this->getVMGroupIDFromUser($userId);
				$query = "INSERT INTO vm (name,game_id,vm_group_id)
					VALUES (:vmName,:gameId,:vmGroupId)";
				$params = array('vmName' => $vmName, 'gameId' => $gameId, 'vmGroupId' => $vmGroupId);
				$result = $this->DB->query($query,$params);
				return $result;
			}
			else{
				throw new Exception("Game Id, User Id and VMName must be not null", 1);
			}
		}

		/**
		 * Creates the abstracted concept "Game" into the DB, which means:
		 * 1. Creates an entry in the Game table and retrieves its new ID
		 * 2. Associates this new Game with the new VM that hosts it
		 * 3. Associates the new Game with the owner User
		 * @param [type] $gameId [description]
		 * @param [type] $userId [description]
		 * @param [type] $vmName [description]
		 * @return int The new Game ID
		 * @throws Exception if GameID, UserID or VMName is null
		 */
		public function addGame($gameId = null, $userId = null, $vmName = null){
			if($gameId !== null && $userId !== null && $vmName !== null){
				$query = "insert into game (name,path,isTemplate,image,internalName)
						select g.name,g.path,false,g.image,g.internalName from game as g where g.id = :id;";
				$params = array('id' => $gameId );
				$this->DB->query($query,$params,true);
				$this->lastInsertId = $this->DB->lastInsertId();

				//insert the referenced VM with the game
				$this->associateGameToVM($this->lastInsertId, $userId, $vmName);

				//now associate this new game with the user
				$this->addGameToUser($this->lastInsertId,$userId);

				//and create a connection group for this new user
				//$this->getConGroup($userId);

				return $this->lastInsertId;
			}
			else{
				throw new Exception("Game Id and User Id must be not null", 1);
			}
		}

		/**
		 * Returns the Connection Group ID of the User with id = $UserId
		 * @param  int $UserId The User ID
		 * @return int The Connection Group ID
		 * @throws Exception if UserID is null
		 */
		public function getConGroup($UserId = null){
			if($UserId !== null){
				$cgrName = 'CONGROUP_USER_' . $UserId;
				$query = "SELECT connection_group_id FROM guacamole_connection_group as cgr
					WHERE cgr.connection_group_name = :name";
				$params = array('name' => $cgrName);
				$result = $this->DB->single($query,$params);

				return $result;
			}
			else{
				throw new Exception("Game Id and User Id must be not null", 1);
			}
		}

		/**
		 * Creates the connection between the new created Game and the guacamole Core.
		 * Calls the stored procedure addCon for retrieving the connection_id in the same query
		 * @param string $ConName The Connection name
		 * @param int $ConGroupId The Connection Group ID
		 * @return int The new Connection ID
		 * @throws Exception if Connection Name or Connection Group ID is null
		 */
		public function addConnection($ConName = null, $ConGroupId = null){
			if($ConName !== null && $ConGroupId !== null){
				$query = "CALL addCon(:name,:id)";
				$params = array('name' => $ConName , 'id' => $ConGroupId);
				$resetConn = true;
				$result = $this->DB->single($query,$params,$resetConn);
				$this->lastInsertId = $result;

				return $this->lastInsertId;
			}
			else{
				throw new Exception("Error adding parameters.", 1);
			}
		}

		/**
		 * Creates the correct permissions for the new Game Connection:
		 * 1. Adds READ permissions for the game owner User to his Connection Group
		 * 2. Adds READ permissions for the game owner User to his Game Connection
		 * @param int $UserId User ID
		 * @param int $ConId User Game Connection ID
		 * @param int $ConGroupId User Connection Group ID
		 * @return int 1 if all went OK or 0
		 * @throws Exception if UserID, Connection ID or Connection Group ID is null
		 */
		public function addConPermissions($UserId = null, $ConId = null, $ConGroupId = null){
			if($UserId !== null && $ConGroupId !== null && $ConId !== null){
				//check if we havent yet a connection group permission
				$query = "SELECT EXISTS(SELECT c.user_id FROM guacamole_connection_group_permission as c
					WHERE c.user_id = :userId)";
				$params = array('userId' => $UserId);
				$result = $this->DB->single($query,$params);

				if(!$result){
					//connection group permissions
					$query = "INSERT INTO guacamole_connection_group_permission (user_id,connection_group_id,permission)
						VALUES(:userId,:ConGroupId,'READ')";
					$params = array('userId' => $UserId , 'ConGroupId' => $ConGroupId);
					$result = $this->DB->single($query,$params);
				}

				//now connection permissions
				$query = "INSERT INTO guacamole_connection_permission (user_id,connection_id,permission)
					VALUES(:userId,:ConId,'READ')";
				$params = array('userId' => $UserId , 'ConId' => $ConId);
				$result = $this->DB->single($query,$params);

				return $result;
			}
			else{
				throw new Exception("Error adding connection permissions.", 1);
			}
		}

		/**
		 * Adds the Connection Parameters to a Connection
		 * @param int $connID The Connection ID
		 * @param array $parameters The Connection Parameters to add
		 * @return string information of the rows inserted
		 * @throws Exception if Connection ID or Parameters is null
		 */
		public function addParameters($connID = null, $parameters = null){
			if($parameters !== null && $connID !== null){
				$rows = 0;
				$resetConn = true;
				foreach ($parameters as $key => $value) {
					$query = "INSERT INTO guacamole_connection_parameter VALUES
						(:id,:key,:value)";
					$params = array('id' => $connID, 'key' => $key, 'value' => $value);
					$this->DB->query($query,$params,$resetConn);
					$rows++;
				}

				return $rows . " rows inserted as params for connection " . $connID;
			}
			else{
				throw new Exception("Error adding connection parameters.", 1);
			}
		}

		/**
		 * Returns the UserID for a Game Given
		 * @param  int $gameId The Game ID
		 * @return int The User ID
		 * @throws Exception if Game ID is null
		 */
		public function getUserIdOfGame($gameId = null){
			if($gameId !== null){
				$query = "SELECT hg.user_id FROM cg_user as u, game as g, has_game as hg
				WHERE g.id = :id AND g.id = hg.game_id";
				$params = array('id' => $gameId );
				$result = $this->DB->single($query,$params);
				return $result;
			}
			else{
				throw new Exception("Game Id must be not null", 1);
			}
		}

		/**
		 * Associates a new Game to his owner User if we have both ID's
		 * @param int $gameId The Game ID
		 * @param int $userId The User ID
		 * @return int The number of rows affected
		 * @throws Exception if UserID or Game ID is null
		 */
		public function addGameToUser($gameId = null,$userId = null){
			$query = "INSERT INTO has_game VALUES (:userId, :gameId)";
			$params = array('userId' => $userId, 'gameId' => $gameId);
			$result = $this->DB->query($query,$params);

			return $result;
		}

		/**
		 * [getAppPath description]
		 * @param  [type] $gameId [description]
		 * @return [type]         [description]
		 */
		public function getAppPath($gameId = null){
			if($gameId !== null){
				$query = "SELECT g.path FROM game as g WHERE g.id = :id";
				$params = array('id' => $gameId );
				$result = $this->DB->single($query,$params,true);
				return $result;
			}
			else{
				throw new Exception("Game Id must be not null", 1);
			}
		}

		/**
		 * Updates the VM IP when started because of they have dynamic dhcp IP's
		 * @param int $ConId The Connection ID
		 * @param string $IP The new IP retrieved by the ShellScript
		 * @return int The number of rows affected
		 * @throws Exception if Connection ID or IP is null
		 */
		public function setVmIp($ConId = null, $IP = null){
			if($ConId !== null && $IP !== null){
				$query = "UPDATE guacamole_connection_parameter SET parameter_value = :ip
					WHERE connection_id = :id AND parameter_name = 'hostname'";
				$params = array('id' => $ConId, 'ip' => $IP );
				$result = $this->DB->query($query,$params);

				return $result;
			}
			else{
				throw new Exception("Connection Id and IP must be not null", 1);
			}
		}

		/**
		 * Returns the Connection ID for the User given
		 * @param  int $UserId User ID
		 * @return int The Connection ID
		 * @throws Exception if UserID is null
		 */
		public function getConId($UserId = null){
			if($UserId !== null){
				$query = "SELECT c.connection_id FROM guacamole_connection_permission as c
					WHERE c.user_id = :id";
				$params = array('id' => $UserId);
				$result = $this->DB->single($query,$params);

				return $result;
			}
			else{
				throw new Exception("Connection Id and IP must be not null", 1);
			}
		}

		public function getGameFromConnection($conName = null){
			if($conName !== null){
				$query = "SELECT g.* FROM game as g, guacamole_connection_permission as cp,
				guacamole_connection as c, cg_user as u, has_game as hg
				WHERE c.connection_name = :conName AND c.connection_id = cp.connection_id 
				AND cp.user_id = u.user_id AND u.user_id = hg.user_id AND hg.game_id = g.id";
				$params = array('conName' => $conName);
				$result = $this->DB->query($query,$params);

				return $result;
			}
			else{
				throw new Exception("Connection Name must be not null", 1);
			}
		}

		/**
		 * Returns last inserted ID in an INSERT or UPDATE query transaction
		 * @return int last inserted ID
		 */
		public function getLastInsertId(){
			return $this->lastInsertId;
		}

	} // END class

 ?>