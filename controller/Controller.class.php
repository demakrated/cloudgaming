<?php 

	abstract class Controller
	{
		protected $attrs;

		function __construct($id = null)
		{
			if($id !== null){
				$this->attrs = $this->initAttrs($id);
			}
			else{
				$this->attrs = array();
			}
		}

		abstract protected function initAttrs($id);
	}
 ?>